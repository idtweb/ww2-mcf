#!/bin/bash -x
cd /app/dist
cp -R example renesas
cp -R /app/src/scripts/* renesas

cd renesas
ant deploy
rm -rf templates
