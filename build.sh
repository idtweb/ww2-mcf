#!/bin/bash -x
mkdir -p /app/build/manifoldcf

cd /app/build
cp -R /opt/manifoldcf/ .

cd manifoldcf/connectors
cp -R /app/src/connectors/idtsolr .

cd /app/build/manifoldcf
ant build

cp -R /app/build/manifoldcf/dist/ /app


