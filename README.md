# ww2-mcf #
Renesas ManifoldCF SOLR Project to crawl and index non-drupal websites in
to OpenSolr. The index is used to augment search pages in renesas.com.

## Features ##

## Building ##
The build and deploy scripts are executed from inside a docker container.

### Set up the container ##

Make sure to copy the appropriate .env file from config before building 
the containers.

`docker-compose up --build -d`

**Setup SOLR for local access**

`docker exec -it ww2-mcf_solr_1 bash`

`cd /opt/solr/server/solr`

`mkdir -p mcf/data mcf/conf`

`cp /app/config/solr/* mcf/conf/`

`exit`

Visit the solr console and add a new core 'mcf'

https://localhost:8983/solr

name: mcf
instanceDir: mcf
dataDir: data

Make sure the mcf index is setup correctly:

http://localhost:8983/solr/mcf/schema

http://localhost:8983/solr/mcf/select?q=*:*

**Build ManifoldCF and custom connectors**

`docker exec -it ww2-mcf_java_1 bash`

`cd /app `

`./build.sh`

**Setup for deployment**

`./setup.sh`

**Start ManifoldCF**

`cd /app/dist/renesas`

`./start.sh &`

This should start the manifold CF cleanly and without errors.

Visit the manifold CF to manually setup the jobs:

http://localhost:8345/mcf-crawler-ui

UN: admin
PW: admin

**Setup default set of jobs manually**

`cd /app/dist/renesas`

`./run-script.sh create-repository-connection.mcf`

This will create a default set of connections and jobs and will start them automatically. Edit the create-repository-connection.mcf script to make necessary changes.

The create-repository-connection.mcf is generated from a template from ~/src/scripts/templates/create-repository-connection.tmpl.mcf

**Clean up the installation**

`./clean.sh`

**
