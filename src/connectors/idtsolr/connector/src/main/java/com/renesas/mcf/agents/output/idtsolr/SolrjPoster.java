package com.renesas.mcf.agents.output.idtsolr;

import org.apache.manifoldcf.agents.system.Logging;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.util.Map;

/**
 * Posts an input stream to SOLR using Solrj
 */
public class SolrjPoster {

    String url;

    HttpSolrClient solr = null;

    public SolrjPoster(String url) {
        this.url = url;
        this.solr = new HttpSolrClient.Builder(url).build();
        solr.setParser(new XMLResponseParser());
        if (Logging.ingest.isInfoEnabled())
            Logging.ingest.info("Initialized a Solrj Poster for URL: " + url);
    }

    public void checkPost() {

    }

    public void shutdown()
    {
        if (solr != null) {
            try {
                solr.close();
            } catch (IOException ex) {
                Logging.ingest.error(ex.toString());
            }
            solr = null;
        }
    }

    public void add(Map<String, String> entries) throws IOException, SolrServerException {
        SolrInputDocument doc = new SolrInputDocument();
        String content = new String();
        for(Map.Entry<String,String> entry : entries.entrySet())
        {
            doc.addField(entry.getKey(), entry.getValue());
            content = content + entry.getValue() + "\n\n";
        }
        doc.addField("content", content);
        solr.add(doc);
        solr.commit();
    }
}
