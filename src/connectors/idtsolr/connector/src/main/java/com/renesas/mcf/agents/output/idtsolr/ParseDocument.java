package com.renesas.mcf.agents.output.idtsolr;

import org.apache.manifoldcf.agents.interfaces.RepositoryDocument;
import org.apache.manifoldcf.agents.system.Logging;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.solr.common.SolrInputDocument;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/*
 * Parse and index forum pages.
 */
public class ParseDocument {
    String url;
    InputStream document;

    Document doc;

    Map<String, String> map;

    public void log(String key, String value) {
        if (Logging.ingest.isInfoEnabled()) {
            Logging.ingest.info("[ParseDocument] " + key + " = " + value);
        }
    }

    public void error(String msg) {
        Logging.ingest.error("[ParseDocument] " + msg);
    }

    public void fetchMeta() {
        Elements metaTags = doc.getElementsByTag("meta");
        for (Element metaTag : metaTags) {
            String content = metaTag.attr("content");
            String name = metaTag.attr("name");
            log("[" + name + " ( meta." + name + ")]", content);
            map.put(name, content);
        }
    }

    public void fetchAttr(String field, String selector, String attr) {
        Element el = doc.select(selector).first();
        if (el != null) {
            log("[" + field + " (" + selector + ":" + attr + ")]", el.attr(attr));
            map.put(field, el.attr(attr));
        }
    }

    public boolean fetch(String field, String selector) {
        Element el = doc.select(selector).first();
        if (el != null) {
            log("[" + field + " (" + selector + ")]", el.text());
            map.put(field, el.text());
            return true;
        }
        return false;
    }

    public Map<String, String> getMap() {
        return map;
    }

    private void parseForum(String url, InputStream document) {
        map.put("site", "RenesasRulz");
        map.put("ts_url", url);
        if (url.contains("japan.renesasrulz.com")) {
            if (url.contains("/rulz-chinese/")) {
                map.put("ts_job", "ds_forum_zh");
            } else {
                map.put("ts_job", "ds_forum_ja");
            }
        } else {
            map.put("ts_job", "ds_forum_en");
        }

        try {
            this.doc = Jsoup.parse(document, "UTF-8", url);
            map.put("ts_title", doc.title());
            fetchAttr("ts_url", "head link[rel=canonical]", "href");
            fetch("ts_container", ".hierarchy .container");
            fetch("ts_application", ".hierarchy .application");
            fetch("ts_post_author", ".thread-start .content .author .user-name");
            fetchAttr("ts_post_author_profile_url", ".thread-start .content .author .user-name a", "href");
            fetchAttr("ts_post_author_profile_avatar", ".thread-start .content .author .avatar img", "src");
            fetchAttr("ts_post_date", ".thread-start .content .author .post-date time", "datetime");
            fetch("ts_post", ".thread-start .content .content");
            fetchAttr("entity_id", ".thread-start .content.full", "id");

            String id;
            if (map.get("entity_id") != null) {
                map.put("entity_type", "forum_post");
                // set the unique id
                // as site + entity_type + entity_id
                id = map.get("site") + " - " + map.get("entity_type") + " - " + map.get("entity_id");
            } else {
                id = map.get("site") + " - " + url;
            }

            map.put("id", id);
        } catch (IOException ex) {
            error(ex.toString());
        }
    }

    private void parseFaqKb(String url, InputStream document) {
        map.put("site", "FAQ");

        if (url.contains("ja-support.renesas.com")) {
            map.put("ts_job", "ds_faq_ja");
        } else if (url.contains("zh-support.renesas.com")) {
            map.put("ts_job", "ds_faq_zh");
        } else {
            map.put("ts_job", "ds_faq_en");
        }

        try {
            this.doc = Jsoup.parse(document, "UTF-8", url);

            String post = doc.text();
            String ts_url = url;

            if (url.contains("file:")) {
                Element ahref = doc.select("a[ng-bind-html=KB.Article.Url]").first();
                if (ahref != null) {
                    ts_url = ahref.attr("href");
                    map.put("ts_url", ts_url);
                }
            } else {
                map.put("ts_url", url);
            }

            // page title
            Element title = doc.select("h2[ng-bind-html=KB.Article.Name]").first();
            if (title != null) {
                map.put("ts_title", title.text());
            }

            Element question = doc.select("div.questionText").first();
            if (question != null) {
                map.put("ts_post_question", question.text());
            }

            Element answer = doc.select("div.answerText").first();
            if (answer != null) {
                map.put("ts_post_answer", answer.text());
            }

            Element suitableProducts = doc.select("div.suitableProducts table").first();
            if (suitableProducts != null) {
                map.put("ts_post_suitable_products", suitableProducts.text());
            }

            map.put("ts_post", post);

            String id;
            id = map.get("site") + " - " + ts_url;

            map.put("id", id);
        } catch (IOException ex) {
            error(ex.toString());
        }
    }

    private void parseTools(String url, InputStream document) {
        map.put("site", "TeamSupport");
        map.put("ts_url", url);
        if (url.contains("/ja-JP/")) {
            map.put("ts_job", "ds_tools_ja");
        } else if (url.contains("/zh-CN/")) {
            map.put("ts_job", "ds_tools_zh");
        } else {
            map.put("ts_job", "ds_tools_en");
        }

        try {
            this.doc = Jsoup.parse(document, "UTF-8", url);

            map.put("ts_title", doc.title());
//            fetch("ts_title", ".manual h1");
            fetch("ts_post", ".manual");

            String id;
            id = map.get("site") + " - " + url;

            map.put("id", id);
        } catch (IOException ex) {
            error(ex.toString());
        }
    }

    public ParseDocument(String url, InputStream document) {
        this.url = url;
        this.document = document;

        this.map = new HashMap<String, String>();

        log("URL", url);

        if (url.contains("renesasrulz.com")) {
            parseForum(url, document);
        }

        else if (url.contains("tool-support.renesas.com")) {
            parseTools(url, document);
        }

        else if (url.contains("support.renesas.com")) {
            parseFaqKb(url, document);
        }

        else {
            error("Unrecognized URL ... "+url);
            return;
        }

        log("id", map.get("id"));
    }

    public void build(SolrInputDocument doc) {
        String content = new String();
        for(Map.Entry<String,String> entry : map.entrySet())
        {
            doc.addField(entry.getKey(), entry.getValue());
            content = content + entry.getValue() + "\n\n";
        }
        doc.addField("content", content);
    }
}
