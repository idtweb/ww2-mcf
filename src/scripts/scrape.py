import requests
import time
import os


class KbScrape:
    """Scrape renesas knowledgeBase articles for ManifoldCF"""

    base = 'https://{}-support.renesas.com/knowledgeBase/'
    apiBase = 'https://{}-support.renesas.com/api/KnowledgeBase/'
    output = 'kb-{}-articles'
    wait = 5  # seconds

    def __init__(self, key):
        self.base = self.base.format(key)
        self.apiBase = self.apiBase.format(key)
        self.output = self.output.format(key)
        if not os.path.exists(self.output):
            os.makedirs(self.output)

    def categories(self):
        r = requests.get(self.apiBase + 'GetCategories')
        j = r.json()

        print("Starting to scrape from {} - saving to {}".format(self.apiBase, self.output))
        print("-----------------------------")

        for x in j['Categories']:
            print(x['CategoryID'], x['CategoryName'])
            self.category_content(x['CategoryID'])

        print("-----------------------------\n")

    def category_content(self, ID):
        r = requests.get(self.apiBase + '/GetCategoryContent?categoryID=' + str(ID))
        j = r.json()
        for x in j['SubCategories']:
            print(x['CategoryID'], x['CategoryName'])
            self.sub_category_content(x['CategoryID'])

    def sub_category_content(self, ID):
        r = requests.get(self.apiBase + '/GetSubCategoryContent?subcategoryID=' + str(ID))
        j = r.json()
        for x in j['Articles']:
            print(x['ArticleID'], x['ArticleName'])
            self.knowledge_base_article(x['ArticleID'])
            time.sleep(self.wait)

    def knowledge_base_article(self, ID):
        url = self.apiBase + '/GetKnowledgeBaseArticle?kbid=' + str(ID) + "&searchText="
        r = requests.get(url)
        j = r.json()
        for x in j['ArticleBodies']:
            fn = self.output + "/" + str(ID) + ".html"
            f = open(fn, "w")
            fmt = """
            <a ng-bind-html="KB.Article.Url" href="{url}"></a>
            <h2 ng-bind-html="KB.Article.Name">{name}</h2>{body}
            """
            f.write(fmt.format(url=self.base + str(ID), name=j['Article']['Name'], body=x['ArticleBody']['Description']))
            f.close()


languages = ["en", "ja", "zh"]
for lang in languages:
    kb = KbScrape(lang)
    kb.categories()
